# -*- coding: utf-8 -*-
from peewee import *
import time

db = SqliteDatabase('chatbot.db')

class BaseModel(Model):
    """A base model that will use our MySQL database"""
    class Meta:
        database = db

class Item(BaseModel):
    name = TextField(verbose_name='备注')
    user = TextField(verbose_name='临时账号')
    isclose = BooleanField(verbose_name='是否禁止',default=0)
    lastdate = DoubleField(verbose_name='上次聊天时间',default=time.time() - 60*60*24*3)
    num = IntegerField(verbose_name='聊天计数',default=0)
    # 0/2女 1男
    sex = IntegerField(verbose_name='性别')
    # 0无
    link = IntegerField(verbose_name='套路',default=0)
    # 0无
    link_step = IntegerField(verbose_name='套路步骤',default=0)

class Info(BaseModel):
    id = IntegerField(verbose_name='唯一标识',default=0)
    isclose = BooleanField(verbose_name='是否禁止',default=False)

class DB:
    @classmethod
    # 数据检索 执行啥呢
    # 0 正常执行
    # 1 循环计数 执行套路
    # 2 距离上次太久 第一次聊天
    # 3 不处理, 已经关闭
    def dataResponse(self, msg):
        ret = 0

        try:
            person = Item.get(Item.user == msg['FromUserName'])
        except:
            person = Item.create(name = msg['User']['RemarkName'],
                                user = msg['FromUserName'],
                                sex = msg['User']['Sex'])

        isFirst = False
        if (time.time() - person.lastdate) > 60 * 60 * 24 * 3 - 10:
            isFirst = True

        # 自动增长次数, time更新
        person.num = person.num + 1;
        person.lastdate = time.time()
        person.save()

        if isFirst:
            return 2
        #
        # # 是否已经关闭
        # info = Info.get(Info.id == 0)
        # if info.isclose == True:
        #     return 3
        #
        # # 单人关闭
        # if person.isclose == True:
        #     return 3

        return ret