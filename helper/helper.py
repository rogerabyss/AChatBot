#coding:utf-8
import itchat
from db.db import *

class Helper:
    @classmethod
    def deel(self, action, person):
        print(u'收到指令: %s' % action)

        # 打开
        for word in [u'你好小超',u'小超出来',u'出来小超']:
            if word == action:
                Helper.open(person=person)

        # 关闭
        for word in [u'再见小超',u'拜拜小超',u'滚']:
            if word == u'滚':
                itchat.send(u'[小超]: 你对可爱的我这么凶，良心不会痛吗？', person)
            if word == action:
                Helper.close(person=person)

        # 工作相关
        for word in [u'Office',u'office']:
            if word == action:
                Helper.office(person=person)

        # 帮助
        for word in [u'Help',u'help']:
            if word == action:
                Helper.office(person=person)


    @classmethod
    def words(self):
        return [u'你好小超',u'小超出来',u'出来小超',u'再见小超',u'拜拜小超']

    @classmethod
    def hello(self, sex):
        if sex == 1:
            return [u'''
小哥哥，你好!

我是小超, 主人在忙~
我可以陪你聊天的呀！

常用指令

和我聊天: 你好小超
关闭我: 拜拜小超(滚)
工作问题: Office
帮个忙: Help''']

        else:
            return [u'''
小姐姐，你好！

我是小超, 主人在忙~
我可以陪你聊天的呀！

常用指令

和我聊天: 你好小超
关闭我: 拜拜小超(滚)
工作问题: Office
帮个忙: Help''']


    @classmethod
    def open(self, person):
        if person == itchat.search_friends()['UserName']:
            itchat.send(u'[小超]: 我来啦，我来啦', 'filehelper')
            try:
                info = Info.get(Info.id == 0)
            except:
                info = Info.create()

            info.isclose = False
            info.save()
        else:
            itchat.send(u'[小超]: 我来啦，我来啦', person)
            person = Item.get(Item.user == person)

            person.isclose = False
            person.save()


    @classmethod
    def close(self, person):
        if person == itchat.search_friends()['UserName']:
            itchat.send(u'[小超]: 那我走了！', 'filehelper')
            try:
                info = Info.get(Info.id == 0)
            except:
                info = Info.create()

            info.isclose = True
            info.save()
        else:
            itchat.send(u'[小超]: 那我走了！', person)
            person = Item.get(Item.user == person)

            person.isclose = True
            person.save()

    @classmethod
    def office(self, person):
        itchat.send(u'iOS 构建与版本分发: (内网登录 http://192.168.3.25:8111/)', person)
        itchat.send(u'iOS 新需求 至WSS (内网登录 http://47.98.116.27/wui/main.jsp)', person)
        itchat.send(u'支付方式和动态首页布局配置: gitlab-json', person)

        itchat.send(u'其他问题反馈: roger_ren@qq.com', person)

    @classmethod
    def help(self, person):
        itchat.send(u'视频会员(腾讯/爱奇艺)', person)
        itchat.send(u'京东Plus会员专享价代购', person)
        itchat.send(u'设计师(五网通用)素材代下(ip太多会被封号)', person)
        itchat.send(u'MacOS 各种破解软件', person)

        itchat.send(u'借钱 -- 因为主人太穷，借钱业务暂时关闭', person)