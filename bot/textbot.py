# encoding=utf8

import itchat
from itchat.content import TEXT
from ai.tuling import Tuling
from helper.helper import Helper
from db.db import DB
from db.db import *
from random import choice

class TextBot:
    @classmethod
    def register(self):
        # 注册消息处理 (文本消息)
        # 群聊消息
        @itchat.msg_register(itchat.content.TEXT, isGroupChat=True)
        def print_content(msg):
            # 等待完善
            print(msg.text)

            if(msg.isAt):
                pass

        # 私人消息
        @itchat.msg_register(itchat.content.TEXT, isGroupChat=False)
        def print_content(msg):
            # 接收消息-接收的人
            ToUserName = msg['ToUserName']
            # 接收消息-发送的人
            FromUserName = msg['FromUserName']

            if (ToUserName == 'filehelper') & (FromUserName == itchat.search_friends()['UserName']):
                Helper.deel(msg.text, FromUserName)
                # 如果是自己發的消息 不處理
                # 如果是[小超]自己的消息, 不处理
            elif ((FromUserName != itchat.search_friends()['UserName']) | (msg.text.startswith(u'[小超]') == False)):

                itchat.send(u'你好, 这个账号已经很少使用, 请参考朋友圈添加新微信!', FromUserName)
