# encoding=utf8

import itchat
from itchat.content import *

class AnyBot:
    @classmethod
    def register(self):
        # 位置
        @itchat.msg_register(itchat.content.MAP)
        def print_content(msg):
            print(msg['Text'])

        # 名片
        @itchat.msg_register(itchat.content.CARD)
        def print_content(msg):
            print(msg['Text'])

        # 通知
        @itchat.msg_register(itchat.content.NOTE)
        def print_content(msg):
            print(msg['Text'])

        # 分享
        @itchat.msg_register(itchat.content.SHARING)
        def print_content(msg):
            print(msg['Text'])

        # 图片
        @itchat.msg_register(itchat.content.PICTURE)
        def print_content(msg):
            return '欺负我不会发图是吧'

        # 语音
        @itchat.msg_register(itchat.content.RECORDING)
        def print_content(msg):
            return '欺负我不会发语音是吧'

        # 附件
        @itchat.msg_register(itchat.content.ATTACHMENT)
        def print_content(msg):
            print(msg['Text'])

        # 视频
        @itchat.msg_register(itchat.content.VIDEO)
        def print_content(msg):
            print(msg['Text'])

        # 好友邀请
        @itchat.msg_register(itchat.content.FRIENDS)
        def print_content(msg):
            print(msg['Text'])

        # 系统消息
        @itchat.msg_register(itchat.content.SYSTEM)
        def print_content(msg):
            print(msg['Text'])