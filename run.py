# encoding=utf8

import itchat
from bot.anybot import AnyBot
from bot.textbot import TextBot
from db.db import *
from helper.helper import Helper

# 链接数据库
db.connect()
# 创建新的表
db.create_tables([Item, Info])

# 注册消息
AnyBot.register()
# 注册消息回复
TextBot.register()

# 自动登录(保存记录)
itchat.auto_login(hotReload=True,enableCmdQR=2)
# 开始运行
itchat.run()

# 打开自动回复
# Helper.open(itchat.search_friends()['UserName'])
